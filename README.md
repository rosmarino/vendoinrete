# vendoinrete

sito per la promozione del servizio di creazione di e-commerce

---

## step-by-step

1. creazione del progetto
    - crea nuovo progetto in GitLab (con solo README.md)
    - apri finestra Visual Code
    - apri Terminal
    - `cd my-directory`
    - `git clone git@gitlab.com:rosmarino/vendoinrete.git`
    - salva workspace

2. installazone Jekyll, da Terminale nella directory del sito:
    ```
    bundle init
    bundle add jekyll
    bundle exec jekyll new --force --skip-bundle .
    bundle install
    ```
    - costruisci sito in localhost:
    `bundle exec jekyll serve`
    - verificare che sia tutto ok come da istruzioni che appiaono, poi terminare server con ctr+c 

3. creazione struttura directory:
    - _data
    - _includes
        - _includes/legos
    - _layouts
    - _pages
    - assets
        - assets/CSS
        - assets/JS
        - assets/images
        - assets/icons
    - collections
        - colections/_legos
        - collections/_faq

4. pulzia:
    - spostare *_post* in *collections*
    - eliminare:
        - about.markdown
        - index.markdown
    - modificare file *.gitignore* aggungendo due righe:
    ```
    .DS_Store
    vendoinrete.code-workspace
    ```

5. creazione dei file principali:
    - in *_layouts* creare il file *default.html*, con contenuto:
    ```
    <!DOCTYPE HTML>
    <html lang="it">
    <!-- Head -->
    <body>
    <!-- Header -->
    <!-- Main -->
    {{ content -}}
    <!-- Footer -->
    <!-- End of page JS scripts -->
    </body>
    </html>
    ```
    - in *_pages* creare il file *index.md*, con contenuto:
    ```
    ---
    layout: default
    permalink: /
    title: ""
    description: ""
    canonical: ""
    ---
    # Hello world

    It is a beautyful day!
     ```

6. confgurazione di **_config.yml**:
    - toglere quello che si vuole dalle info che comnciano con #
    - togliere il tema *minima*, basta elminare la riga
    - togliere: *twitter_username: jekyllrb* e *github_username:  jekyll*
    - aggiungere i settaggi di base:
        ```
        lang: it_IT
        timezone: Europe/Rome
        include: ['_pages']
        ```
    - aggungere i settaggi delle collezioni:
        ```
        collections_dir: collections
        collections:
        legos:
            sort_by: order
        posts:
            permalink: /blog/:title:output_ext
        faq:
            output: true
            permalink: /faq/:title:output_ext
            sort_by: order
        ```
È ora di fare una verifca facendo ripartire il server con `bundle exec jekyll serve`.

In realtà c'è un errore, il server ci dice: *Build Warning: Layout 'post' requested in _posts/2020-05-15-welcome-to-jekyll.markdown does not exist*

Lo aggiustiamo, creando il file *post.html* i *_layouts*, con contenuto:
```
---
layout: default
---
```
Di fatto il layout dice solo di usare il layout *default*, per ora va bene, riempiremo quando sarà necessario.

Questo fatto, il sito è ok, ma c'è ancora una pulizia, quella el *Gemfile*

7. Pulizia del **Gemfile**
- eliminamo la riga che contiene `gem "minima", "~> 2.5"`, eventuali righe di commento e salviamo
- chiudamo il server con ctrl+c
- aggiorniamo da Terminale con `bundle install`

Magari è il momento di fare un commit generale e push, così il git online si aggiorna

8. aggiunta del foglio di stile
- in *assets/CSS* aggungere *main.scss*
- in  *assets/CSS* importare w3.css

9. aggiunta di file importanti in *_includes*:
    - head.html
    - header.html
    - footer.html

Senza copiare qui il contenuto, si possono vedere direttamente i file.

Poi va fatta la loro inclusione nel file deafult.html