---
layout: default
permalink: /
title: "homepage"
description: ""
canonical: ""
---
<main>
{%- assign legos = site.legos | where: "page", "home" -%}
{% for lego in legos %}
  {%- if lego.include != null -%}
    {% include {{ lego.include }} -%}
  {% endif %}
{% endfor %}
</main>